start=$SECONDS
scripts/4.prepare-infrastructure.sh
scripts/5.deploy-cluster.sh
duration=$((SECONDS-start))

if (( $duration > 3600 )) ; then
    let "hours=duration/3600"
    let "minutes=(duration%3600)/60"
    let "seconds=(duration%3600)%60"
    echo "Cluster deployed in $hours hour(s), $minutes minute(s) and $seconds second(s)"
elif (( $duration > 60 )) ; then
    let "minutes=(duration%3600)/60"
    let "seconds=(duration%3600)%60"
    echo "Cluster deployed in $minutes minute(s) and $seconds second(s)"
else
    echo "Cluster deployed in $duration seconds"
fi

