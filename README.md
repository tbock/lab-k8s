# Getting Started


> Note: this lab has been built to run on a local Ubuntu terminal on Windows 10. For instructions on how to set this up, refer to [Install Ubuntu on Windows 10](https://tutorials.ubuntu.com/tutorial/tutorial-ubuntu-on-windows#0)

## 1. Clone the repository to your local /home directory
```bash
~$ cd ~
~$ git clone https://bitbucket.org/tbock/lab-k8s.git

# or if you have creds / ssh key: git clone git@bitbucket.org:tbock/lab-k8s.git
```

## 2. Prepare the environment
```bash
~$ cd lab-k8s
~/lab-k8s$ ./1.prepare-local-env.sh
```

#### Configure the AWS Variables File
```
~/lab-k8s$ vim ~/aws_vars.yaml
aws_access_key: <AWS Access Key>
aws_secret_key: <AWS Secret Key>
```
#### Configure the AWS CLI
```
~/lab-k8s$ aws configure
AWS Access Key ID [None]: <AWS Access Key>
AWS Secret Access Key [None]: <AWS Secret Key>
```

#### Create your SSH key to be used for AWS EC2 instances
```
~$ ssh-keygen -q -t rsa -C "" -N "" -f ~/.ssh/id_rsa -y
```

## 3. Deploy the cluster
```
~/lab-k8s$ ./2.deploy-k8s-cluster.sh
```

## 4. Deploy a sample application
```
~/lab-k8s$ ./3.deploy-sample-application.sh
```

## 5. Destroy the cluster
```
~/lab-k8s$ ./4.destroy-cluster.sh
```

#### Note: when manually deleting the cluster from AWS, follow the below order.
1. Delete the EC2 instances
2. Delete the ELB
3. Delete the VPC
4. Release the remaining Elastic IPs
5. Delete the AWS instance profiles
```
~$ aws iam delete-instance-profile --region us-east-2 --instance-profile-name kube_k8slab_master_profile
~$ aws iam delete-instance-profile --region us-east-2 --instance-profile-name kube_k8slab_node_profile
```
