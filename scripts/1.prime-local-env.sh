sudo apt-get update

sudo apt-get install vim unzip awscli \
python python-boto python-botocore \
python-boto3 python-pip -y

cp ~/lab-k8s/ansible/vars/aws_vars.yaml ~/
